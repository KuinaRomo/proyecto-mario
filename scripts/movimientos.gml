contadorBala++;
//Compruebas si se aprieta una tecla y se convierte el resultado en un número entre 0 y 1. Si es 0 no se mueve, si es 1 o -1 si
key_right = keyboard_check(vk_right);
//key left en negativo para que vaya hacia la izquierda
key_left = -keyboard_check(vk_left);
key_jump = keyboard_check_pressed(vk_space);

//Como left es negativo move sera 0 siempre que no se apriete nada o que se aprieten los dos botones a la vez
if(key_jump == 1){
    grounded = false;
}

if(grounded){
    move = key_left + key_right;
    hsp = move * movespeed;
}

if(move != 0 && place_meeting(x, y+1, obj_suelo)){
    contador++;
}

if(vsp < 10){
    vsp += grav;
}

if(move == 0){
   contador = 0;
}


if(place_meeting(x, y+1, obj_suelo) && key_jump != 1){
    grounded = true;
    saltoHecho = false;
    saltando = true;
}

if(place_meeting(x, y+1, obj_suelo)){
    vsp = key_jump * -jumpspeed;
}

if(!place_meeting(x,y+1,obj_suelo) && saltando == true){
    if (contador == 0){
        hsp = 0;
        contador = 0;
        saltando = false;
    }
    else if (contador < 10){
        hsp = hsp_jump_constant_small*move;
        contador = 0;
        saltando = false;
    } 
    else{
        hsp = hsp_jump_constant_big*move;
        contador = 0;
        saltando = false;
    }
}


//Colision horizontal
if(place_meeting(x+hsp, y, obj_suelo)){
    while(!place_meeting(x + sign(hsp), y, obj_suelo)){
        x += sign(hsp);
    }
    hsp = 0;
    contador = 0;
}
x += hsp;
if(x < 55){
    x = 55;
}
else if(x > 900){
    x = 900;
}
//Salto
if(place_meeting(x, y+vsp, obj_suelo)){
    while(!place_meeting(x, y+sign(vsp), obj_suelo)){
        y += sign(vsp);
    }
    vsp = 0;
}

y += vsp;

if(move == 0){
    contador_idle++;
}

if(vsp != 0 || !grounded){
    sprite_index = spr_salto;
    contador_idle = 0;
}

else{
    if(hsp != 0){
        sprite_index = spr_andar;
        image_speed = 0.8;
    }
    else{
        if(contador_idle < 100){
             sprite_index = spr_disparo;
        }
        else{
            sprite_index = spr_idle;
            image_speed = 0.2;
        }       
   }
}

if(vsp == 0){
    if(move == -1){
        image_xscale = move;
        contador_idle = 0;
        global.dir_per = -1;
    }
    else if(move == 1){
        image_xscale = 1;
        contador_idle = 0;
        global.dir_per = 1;
    }
}

if(move != 0 && instance_exists(obj_platform)){
    instance_destroy(obj_platform);
}

if(sonido_salto == false && key_jump == 1){
    audio_play_sound(snd_salto, 1, false);
    sonido_salto = true;
}

if(vsp == 0){
    sonido_salto = false;
}

